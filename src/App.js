import "./App.css";
import FormInputColor from "./components/FormInputColor";
import RefHook from "./components/ref";
import CallbackHook from "./components/callback";
import Counter from "./components/memo";
import ContextHook from "./components/contexthook";
import ReducerHook from "./components/reducer";
import UseForm from "./components/customhook";
import HOC from "./components/hoc";

function App() {
  return (
    <div className="App">
      {/*  FormInputColor */}
      <FormInputColor />
      {/* Ref Hook */}
      <RefHook />
      {/* CallbackHook */}
      <CallbackHook />
      {/* MemoHook  */}
      <Counter />
      {/* ContextHook */}
      <ContextHook />
      {/* ReducerHook */}
      <ReducerHook />
      {/* CustomHook */}
      <UseForm />
      {/* HOC */}
      <HOC />
    </div>
  );
}

export default App;

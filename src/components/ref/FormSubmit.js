import React, { useRef, useState } from "react";
const FormSubmit = () => {
  const [text, setText] = useState("");
  const inputRef = useRef(null);

  console.log("Change comp when set state value input");
  // setState make re-render comp when input change value
  // useRef no re-render comp when inputRef change
  const handleSubmit = (e) => {
    e.preventDefault();
    console.log({ text, inputRef: inputRef.current.value });
  };

  return (
    <div>
      <form>
        <input
          type="text"
          onChange={(e) => setText(e.target.value)}
          value={text}
        />
        <input type="text" ref={inputRef} />
        <button onClick={handleSubmit}>Submit</button>
      </form>
    </div>
  );
};
export default FormSubmit;

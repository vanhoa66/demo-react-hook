import React, { forwardRef } from "react";

const Input = (props, ref) => <input type="text" ref={ref} />;

export default forwardRef(Input);

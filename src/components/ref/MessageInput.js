import React, { useRef, useState } from "react";
const MessageInput = () => {
  const [message, setMessage] = useState("");
  const sentMessage = useRef(0);

  const sendMessage = () => {
    if (sentMessage.current === 2) {
      return alert("Message Limit Reached");
    }

    sentMessage.current += 1;
    console.log({ sentMessage });
    //code to handle sending message
  };

  return (
    <div>
      <input onChange={(e) => setMessage(e.target.value)} value={message} />
      <button onClick={sendMessage}>Send</button>
    </div>
  );
};
export default MessageInput;

import React, { useRef, useEffect } from "react";
// import Input from "./Input";
// import MessageInput from "./MessageInput";
import FormSubmit from "./FormSubmit";

function RefHook() {
  const inputRef = useRef(null);

  useEffect(() => {
    if (inputRef.current) {
      inputRef.current.focus();
    }
    // console.log({ inputRef });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div>
      {/* <h3>Ref Hook</h3> */}
      {/* useRef */}
      {/* <MessageInput /> */}
      {/* forwardRef  */}
      {/* <h3>ForwardRef Hook</h3>
      <Input ref={inputRef} /> */}
      {/* Ref FormSubmit */}
      <h3>Ref FormSubmit</h3>
      <FormSubmit />
    </div>
  );
}

export default RefHook;

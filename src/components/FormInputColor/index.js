import React, { useState, useRef, useEffect } from "react";
import Input from "./Input";
import Button from "./Button";

function FormInputColor() {
  const [color, setColor] = useState("#ffffff");
  const [colorTwo, setColorTwo] = useState("#111111");
  const inputRef = useRef(null);

  const handerChangeColor = () => {
    setColor(`${inputRef.current.value}`);
  };
  useEffect(() => {
    if (inputRef.current) {
      inputRef.current.value = color;
    }
  }, [color]);

  return (
    <div>
      <Input
        ref={inputRef}
        color={color}
        handerChangeColor={handerChangeColor}
      />
      <Button color={color} text="Ref - forwardRef" />
      <input
        type="color"
        value={colorTwo}
        onChange={(e) => setColorTwo(e.target.value)}
      />
      <Button color={colorTwo} text="No Ref - No forwardRef" />
    </div>
  );
}

export default FormInputColor;

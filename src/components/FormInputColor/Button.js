import React from "react";

const Button = ({ color, text }) => (
  <button style={{ backgroundColor: `${color}`, color: "red" }}>
    Button {color} - {text}
  </button>
);

export default Button;

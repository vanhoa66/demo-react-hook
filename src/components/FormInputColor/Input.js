import React, { forwardRef } from "react";

const Input = (props, ref) => {
  console.log({ props });
  return (
    <input
      type="color"
      ref={ref}
      color={props.color}
      onChange={props.handerChangeColor}
    />
  );
};

export default forwardRef(Input);

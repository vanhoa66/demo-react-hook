import React, { Component } from "react";
import ClickCounter from "./ClickCounter";
import HoverCounter from "./HoverCounter";

class HOC extends Component {
  render() {
    return (
      <div>
        <h2>HOC - Higher Order Function </h2>
        <ClickCounter name="VanHoa" />
        <HoverCounter />
      </div>
    );
  }
}

export default HOC;

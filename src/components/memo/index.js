import React, { useState, useMemo } from "react";

function Counter() {
  const [counterZero, setCounterZero] = useState(0);
  const [counterOne, setCounterOne] = useState(1);
  const [counterThree, setCounterThree] = useState(3);
  const [counterFour, setCounterFour] = useState(4);

  const incrementZero = () => {
    setCounterZero(counterZero + 1);
  };

  const incrementOne = () => {
    setCounterOne(counterOne + 1);
  };
  const incrementThree = () => {
    setCounterThree(counterThree + 1);
  };

  const incrementFour = () => {
    setCounterFour(counterFour + 1);
    return true;
    // warning should use callback beacause it return in useMemo
  };
  // const incrementFour = useCallback(() => {
  //   setCounterFour(counterFour + 1);
  // }, [counterFour]);

  const isEvenNoMemo = () => {
    console.log("no use memo");
    return counterZero % 2 === 0;
  };

  const isEven = useMemo(() => {
    console.log("useMemo has depend");
    return counterOne % 2 === 0;
  }, [counterOne]);

  const isOdd = useMemo(() => {
    console.log("useMemo no depend");
    return counterThree % 2 !== 0;
    // no depend run once when render after don't run again when call or comp re-render
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const useMemoReturnFunc = useMemo(() => {
    console.log("useMemo return function");
    return incrementFour;
    // return func as callback no denpend and run again when call or comp re-render
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div>
      <h2>useMemo Hook</h2>
      <div>
        <button onClick={incrementZero}>No useMeno - {counterZero}</button>
        <span>{isEvenNoMemo() ? "Even" : "Odd"}</span>
      </div>
      <div>
        <button onClick={incrementOne}>
          useMemo with depend - {counterOne}
        </button>
        <span>{isEven ? "Even" : "Odd"}</span>
      </div>
      <div>
        <button onClick={incrementThree}>
          useMemo empty depend - {counterThree}
        </button>
        <span>{isOdd ? "Odd" : "Even"}</span>
      </div>
      <div>
        <button onClick={useMemoReturnFunc}>
          useMemo return Function - {counterFour}
        </button>
        <span>{useMemoReturnFunc ? "True" : "False"}</span>
      </div>
    </div>
  );
}

export default Counter;

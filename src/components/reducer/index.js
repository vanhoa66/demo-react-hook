import React, { useReducer } from "react";
import CounterOne from "./CounterOne";
// import CounterTwo from './components/CounterTwo'
// import CounterThree from './components/CounterThree'
// import ComponentA from './components/ComponentA'
// import ComponentB from './components/ComponentB'
import ComponentD from "./ComponentD";
// import DataFetchingOne from './components/DataFetchingOne'
// import DataFetchingTwo from './components/DataFetchingTwo'

const initialState = 0;
const reducer = (state, action) => {
  switch (action) {
    case "increment":
      return state + 1;
    case "decrement":
      return state - 1;
    case "reset":
      return initialState;
    default:
      return state;
  }
};

export const CountContext = React.createContext();

function ReducerHook() {
  const [count, dispatch] = useReducer(reducer, initialState);
  return (
    <CountContext.Provider
      value={{ countState: count, countDispatch: dispatch }}
    >
      <div>
        <h2>useReducer Hook</h2>
        <CounterOne />
        {/* <CounterTwo /> */}
        {/* <CounterThree /> */}
        {count}
        {/* <ComponentA /> */}
        {/* <ComponentB /> */}
        <ComponentD />
        {/* <DataFetchingOne /> */}
        {/* <DataFetchingTwo /> */}
      </div>
    </CountContext.Provider>
  );
}

export default ReducerHook;

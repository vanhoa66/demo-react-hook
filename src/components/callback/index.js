import React, { useState, useCallback } from "react";
import Count from "./Count";
import Button from "./Button";
import Title from "./Title";

function CallbackHook() {
  const [age, setAge] = useState(25);
  const [salary, setSalary] = useState(50000);
  const [noDepend, setNoDepend] = useState(10);
  // const [emptyDepend, setEmptyDepend] = useState(100);
  const [noCallback, setNoCallback] = useState(200);

  const incrementAge = useCallback(() => {
    setAge(age + 1);
  }, [age]);

  const incrementSalary = useCallback(() => {
    setSalary(salary + 1000);
  }, [salary]);

  const incrementNoDepend = useCallback(() => {
    // run only once when click button
    setNoDepend(noDepend + 20);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // const incrementEmptyDepend = useCallback(() => {
  //   // run again when comp re-render
  //   setEmptyDepend(emptyDepend + 200);
  // });

  const incrementNoCallback = () => {
    // run again when call or comp re-render
    setNoCallback(noCallback + 200);
  };

  return (
    <div>
      <Title />
      <Count text="Age" count={age} />
      <Button handleClick={incrementAge}>Increment Age</Button>
      <Count text="Salary" count={salary} />
      <Button handleClick={incrementSalary}>Increment Salary</Button>
      <Count text="NoDepend" count={noDepend} />
      <Button handleClick={incrementNoDepend}>Increment NoDepend</Button>
      {/* <Count text="Empty Depend" count={emptyDepend} />
      <Button handleClick={incrementEmptyDepend}>Increment Empty Depend</Button> */}
      <Count text="No use callback" count={noCallback} />
      <Button handleClick={incrementNoCallback}>Increment No call back</Button>
    </div>
  );
}

export default CallbackHook;

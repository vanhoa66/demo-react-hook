import React from "react";
import ComponentC from "./ComponentC";

export const UserContext = React.createContext();
export const ChannelContext = React.createContext();

function ContextHook() {
  return (
    <div>
      <h2>useContext Hook</h2>
      <UserContext.Provider value={"Van Hoa"}>
        <ChannelContext.Provider value={"ReactPlus"}>
          <ComponentC />
        </ChannelContext.Provider>
      </UserContext.Provider>
    </div>
  );
}

export default ContextHook;
